﻿using System;
using System.Collections.Generic;
using EnjoyCQRS.Events;
using EnjoyCQRS.TestFramework;
using PJMT.SgpPlus.Core.Domain.Aggregates;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using Xunit;
using FluentAssertions;
using static System.Convert;

namespace PJMT.SgpPlus.Core.Domain.Tests.Cenarios.PessoaSpecs
{
    public class Quando_uma_pessoa_já_está_excluída : AggregateTestFixture<Pessoa>
    {
        private static readonly PessoaCriada PessoaCriadaComSucesso = new PessoaCriada(Guid.NewGuid(), "Fulano de Almeida", "22255544411", ToDateTime("28/12/1985"));
        private static readonly PessoaExcluida PessoaExcluidaComSucesso = new PessoaExcluida(PessoaCriadaComSucesso.Id, DateTime.Now);

        protected override IEnumerable<IDomainEvent> Given()
        {
            yield return PrepareDomainEvent.Set(PessoaCriadaComSucesso).ToVersion(1);
            yield return PrepareDomainEvent.Set(PessoaExcluidaComSucesso).ToVersion(2);
        }

        protected override void When()
        {
            AggregateRoot.Excluir();
        }

        [Fact]
        public void Deve_Causar_InvalidOperationException()
        {
            CaughtException.Should().BeOfType<InvalidOperationException>();
        }
    }
}