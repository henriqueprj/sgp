﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;
using EnjoyCQRS.TestFramework;
using FluentAssertions;
using PJMT.SgpPlus.Core.Domain.Aggregates;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using Xunit;

namespace PJMT.SgpPlus.Core.Domain.Tests.Cenarios.PessoaSpecs
{
    public class Quando_alterar_cpf_com_pessoa_excluida : AggregateTestFixture<Pessoa>
    {

        private static readonly PessoaCriada PessoaCriadaComSucesso = new PessoaCriada(Guid.NewGuid(), "João da Silva Sauro", "12345678909", new DateTime(1981, 6, 11));

        protected override IEnumerable<IDomainEvent> Given()
        {
            yield return PrepareDomainEvent.Set(PessoaCriadaComSucesso).ToVersion(1);
            yield return PrepareDomainEvent.Set(new PessoaExcluida(PessoaCriadaComSucesso.AggregateId, DateTime.Now)).ToVersion(2);
        }

        protected override void When()
        {
            AggregateRoot.AlterarCpf("46250742239");
        }

        [Fact]
        public void Deve_causar_InvalidOperationException()
        {
            CaughtException.Should().BeOfType<InvalidOperationException>();
        }
    }
}
