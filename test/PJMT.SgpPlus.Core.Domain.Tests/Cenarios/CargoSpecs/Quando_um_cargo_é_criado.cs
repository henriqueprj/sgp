﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.TestFramework;
using FluentAssertions;
using PJMT.SgpPlus.Core.Domain.Aggregates;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Cargo;
using Xunit;

namespace PJMT.SgpPlus.Core.Domain.Tests.Cenarios.CargoSpecs
{
    public class Quando_um_cargo_é_criado : AggregateTestFixture<Cargo>
    {
        private Guid _id;
        private string _nome;
        private int _quantidade; 

        protected override void When()
        {
            _id = Guid.NewGuid();
            _nome = "Presidente";
            _quantidade = 1;

            AggregateRoot = new Cargo(_id, _nome, _quantidade);
        }

        [Fact]
        public void Deve_constar_o_evento_de_criacao()
        {
            PublishedEvents.Last().Should().BeOfType<CargoCriado>();
        }

        [Fact]
        public void O_evento_CargoCriado_deve_estar_preenchido_corretamente()
        {

            var evento = PublishedEvents.Last().As<CargoCriado>();

            evento.AggregateId.Should().Be(_id);
            evento.Nome.Should().Be(_nome);
            evento.Quantidade.Should().Be(_quantidade);
        }
    }
}
