﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.TestFramework;
using FluentAssertions;
using PJMT.SgpPlus.Core.Domain.Aggregates;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Cargo;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using Xunit;

namespace PJMT.SgpPlus.Core.Domain.Tests.Cenarios.CargoSpecs
{
    public class Quando_um_cargo_é_criado_com_nome_nao_preenchido : AggregateTestFixture<Cargo>
    {
        private Guid _id;
        private string _nome;
        private int _quantidade;

        protected override void When()
        {
            _id = Guid.NewGuid();
            _nome = string.Empty;
            _quantidade = 1;

            AggregateRoot = new Cargo(_id, _nome, _quantidade);
        }

        [Fact]
        public void Deve_causar_ArgumentNullException()
        {
            CaughtException.Should().BeOfType<ArgumentNullException>();
        }
    }
}
