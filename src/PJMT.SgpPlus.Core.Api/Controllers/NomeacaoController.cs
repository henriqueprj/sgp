﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EnjoyCQRS.Messages;
using PJMT.SgpPlus.Core.Api.Models;
using PJMT.SgpPlus.Core.Application.Servidor;

namespace PJMT.SgpPlus.Core.Api.Controllers
{
    [RoutePrefix("v1/nomeacoes")]
    public class NomeacaoController : ApiController
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public NomeacaoController(
            ICommandDispatcher commandDispatcher
            )
        {
            _commandDispatcher = commandDispatcher;
        }

        [Route("nomear")]
        [HttpPost]
        public async Task Nomear(NomearPessoaDto dto)
        {
            var command = new NomearPessoaCommand(dto.IdPessoa, dto.IdCargo);
            await _commandDispatcher.DispatchAsync(command);
        }
    }
}
