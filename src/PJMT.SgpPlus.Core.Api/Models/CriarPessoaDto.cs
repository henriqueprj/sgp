﻿using System;

namespace PJMT.SgpPlus.Core.Api.Models
{
    public class CriarPessoaDto
    {
        public string Nome { get; set; }

        public string Cpf { get; set; }

        public DateTime DataNascimento { get; set; }
    }
}