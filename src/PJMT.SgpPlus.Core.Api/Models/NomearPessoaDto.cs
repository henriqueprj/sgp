﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PJMT.SgpPlus.Core.Api.Models
{
    public class NomearPessoaDto
    {
        public Guid IdPessoa { get; set; }

        public Guid IdCargo { get; set; }
    }
}