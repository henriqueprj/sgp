﻿using System;

namespace PJMT.SgpPlus.Core.Api.Models
{
    public class ExcluirPessoaDto
    {
        public Guid Id { get; set; }
    }
}