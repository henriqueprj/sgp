﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJMT.SgpPlus.Core.Infrastructure.SqlServerEventStore
{
    /// <summary>
    /// Auxilia no gerencimento de estado do objeto de conexão.
    /// </summary>
    public class ConnectionStateManager : IDisposable
    {

        /// <summary>
        /// Retorna o estado inicial da conexão no momento da criação de <see cref="ConnectionStateManager{TConnection}"/>.
        /// </summary>
        public ConnectionState InitialConnectionState { get; }

        /// <summary>
        /// Retorna a instância do objeto de conexão informado na inicialização.
        /// </summary>
        public IDbConnection Connection { get; private set; }

        /// <summary>
        /// Retorna a instância do objeto de transação envolvido na conexão.
        /// </summary>
        public IDbTransaction Transaction { get; private set; }

        /// <summary>
        /// Inicializa a classe com base no objeto <see cref="IDbConnection"/> informado.
        /// </summary>
        /// <param name="connection"></param>
        public ConnectionStateManager(IDbConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));

            Connection = connection;
            InitialConnectionState = connection.State;

            OpenConnectionIfClosed();
        }

        private void OpenConnectionIfClosed()
        {
            if (InitialConnectionState == ConnectionState.Closed)
                Connection.Open();
        }

        /// <summary>
        /// Inicia uma nova transação.
        /// </summary>
        /// <returns></returns>
        public IDbTransaction BeginTransaction()
        {
            EnsureNotDisposed();

            if (Connection.State != ConnectionState.Open)
                throw new InvalidOperationException("A Conexão precisa estar aberta para iniciar uma transação.");

            Transaction = Connection.BeginTransaction();
            return Transaction;
        }

        /// <summary>
        /// Inicia uma nova transação, com um determinado <see cref="IsolationLevel"/>.
        /// </summary>
        /// <param name="il">Nível de isolamento da transação.</param>
        /// <returns></returns>
        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            EnsureNotDisposed();

            if (Connection.State != ConnectionState.Open)
                throw new InvalidOperationException("A Conexão precisa estar aberta para iniciar uma transação.");

            Transaction = Connection.BeginTransaction(il);
            return Transaction;
        }

        /// <summary>
        /// Desfaz as operações realizadas desde o início da transação.
        /// </summary>
        public void Rollback()
        {
            EnsureNotDisposed();

            if (Transaction == null)
                throw new InvalidOperationException("Não há uma transação ativa.");

            Transaction.Rollback();
            Transaction = null;
        }

        /// <summary>
        /// Confirma todas as alterações realizadas desde a abertura da transação.
        /// </summary>
        public void Commit()
        {
            EnsureNotDisposed();

            if (Transaction == null)
                return;

            Transaction.Commit();
            Transaction = null;
        }

        /// <summary>
        /// Fecha a conexão e libera os recursos.
        /// </summary>
        /// <remarks>Não fechará a conexão caso a mesma já encontrava-se aberta no momento da criação do <see cref="ConnectionStateManager"/>.</remarks>
        public void Dispose()
        {
            EnsureNotDisposed();

            if (Connection.State == ConnectionState.Open && InitialConnectionState == ConnectionState.Closed)
            {
                Transaction?.Rollback();
                Connection.Close();
            }

            Connection = null;
            Transaction = null;
        }

        private void EnsureNotDisposed()
        {
            if (Connection == null) throw new InvalidOperationException("O objeto já foi liberado (Disposed).");
        }
    }
}
