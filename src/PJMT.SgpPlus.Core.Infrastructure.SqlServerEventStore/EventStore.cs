﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;
using EnjoyCQRS.EventSource.Storage;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace PJMT.SgpPlus.Core.Infrastructure.SqlServerEventStore
{
    public class EventStore : IEventStore
    {
        class PrivateSetterContractResolver : DefaultContractResolver
        {
            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                var jProperty = base.CreateProperty(member, memberSerialization);
                if (jProperty.Writable)
                    return jProperty;

                jProperty.Writable = (member as PropertyInfo)?.GetSetMethod(true) != null;

                return jProperty;
            }
        }

        private readonly Dictionary<string, Type> _typeCache = new Dictionary<string, Type>();
        private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None };
        private readonly string _connectionString;
        


        private SqlConnection _connection;

        private SqlConnection Connection => 
            _connection ?? (_connection = new SqlConnection(_connectionString));

        private SqlTransaction Transaction { get; set; }

        public EventStore(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString)) throw new ArgumentNullException(nameof(connectionString));
            _connectionString = connectionString;
        }

        private async Task EnsureConnectionOpennedAsync()
        {
            if (Connection.State != ConnectionState.Open)
                await Connection.OpenAsync();
        }

        private void EnsureConnectionOpenned()
        {
            if (Connection.State != ConnectionState.Open)
                 Connection.Open();
        }

        private SqlCommand CreateCommand()
        {
            var command = Connection.CreateCommand();
            command.Transaction = Transaction;
            return command;
        }

        public void BeginTransaction()
        {
            if (Transaction != null) throw new InvalidOperationException("Transaction already established.");

            EnsureConnectionOpenned();
            Transaction = Connection.BeginTransaction();
        }

        public async Task CommitAsync()
        {
            if (Transaction == null) throw new InvalidOperationException("There are no transaction established.");
            Transaction.Commit();
            Transaction = null;
        }

        public void Rollback()
        {
            Transaction?.Rollback();
        }

        public async Task<IEnumerable<IDomainEvent>> GetAllEventsAsync(Guid id)
        {
            using (var command = CreateCommand())
            {
                command.CommandText = "SELECT EventData, Metadata FROM WM.Event WHERE AggregateId = @id ORDER BY Version";
                command.Parameters.AddWithValue("@id", id);

                var beforeConnectionState = Connection.State;
                await EnsureConnectionOpennedAsync();

                var events = new List<IDomainEvent>();

                using (var reader = await command.ExecuteReaderAsync().ConfigureAwait(false))
                {
                    while (reader.Read())
                    {
                        var @event = Deserialize(reader.GetString(0), reader.GetString(1));
                        events.Add(@event);
                    }
                }

                if (beforeConnectionState == ConnectionState.Closed)
                    Connection.Close();

                return events;
            }
        }


        private IDomainEvent Deserialize(string eventData, string metadata)
        {
            var eventClrTypeName = (string)JObject.Parse(metadata).Property("EventClrType").Value;

            Type eventType;
            if (!_typeCache.TryGetValue(eventClrTypeName, out eventType))
            {
                eventType = Type.GetType(eventClrTypeName);
                _typeCache.Add(eventClrTypeName, eventType);
            }

            var settings = new JsonSerializerSettings { ContractResolver = new PrivateSetterContractResolver() };
            var @event = (IDomainEvent)JsonConvert.DeserializeObject(eventData, eventType, settings);

            return @event;
        }

        private Event Serialize(IDomainEvent @event)
        {
            var data = JsonConvert.SerializeObject(@event, SerializerSettings);
            var eventHeaders = new Dictionary<string, object>
            {
                {
                    "EventClrType", @event.GetType().AssemblyQualifiedName
                }
            };
            var metadata = JsonConvert.SerializeObject(eventHeaders, SerializerSettings);

            return new Event
            {
                Id = @event.Id,
                Timestamp = DateTime.UtcNow,
                EventType = @event.GetType().FullName,
                AggregateId = @event.AggregateId,
                Version = @event.Version,
                EventData = data,
                Metadata = metadata,
            };
        }

        public async Task SaveAsync(IEnumerable<IDomainEvent> events)
        {
            using (var command = CreateCommand())
            {
                command.CommandText = "INSERT INTO WM.Event (Id, Timestamp, EventType, AggregateId, Version, EventData, Metadata) VALUES (@Id, @Timestamp, @EventType, @AggregateId, @Version, @EventData, @Metadata)";

                var paramId = command.Parameters.Add("@Id", SqlDbType.UniqueIdentifier);
                var paramTimestamp = command.Parameters.Add("@Timestamp", SqlDbType.DateTime);
                var paramEventType = command.Parameters.Add("@EventType", SqlDbType.VarChar, 100);
                var paramAggregateId = command.Parameters.Add("@AggregateId", SqlDbType.UniqueIdentifier);
                var paramVersion = command.Parameters.Add("@Version", SqlDbType.Int);
                var paramEventData = command.Parameters.Add("@EventData", SqlDbType.NVarChar, -1);
                var paramMetadata = command.Parameters.Add("@Metadata", SqlDbType.NVarChar, -1);

                var beforeConnectionState = Connection.State;
                await EnsureConnectionOpennedAsync();

                foreach (var @event in events)
                {
                    var eventSerialized = Serialize(@event);

                    paramId.Value = eventSerialized.Id;
                    paramTimestamp.Value = eventSerialized.Timestamp;
                    paramEventType.Value = eventSerialized.EventType;
                    paramAggregateId.Value = eventSerialized.AggregateId;
                    paramVersion.Value = eventSerialized.Version;
                    paramEventData.Value = eventSerialized.EventData;
                    paramMetadata.Value = eventSerialized.Metadata;

                    await command.ExecuteNonQueryAsync();
                }

                if (beforeConnectionState == ConnectionState.Closed)
                    Connection.Close();
            }
        }

        public void Dispose()
        {
            Transaction?.Rollback();
            Connection?.Dispose();

            Transaction = null;
            _connection = null;
        }
    }


}
