﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJMT.SgpPlus.Core.Infrastructure.SqlServerEventStore
{
    public class Event
    {
        public Guid Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string EventType { get; set; }
        public Guid AggregateId { get; set; }
        public int Version { get; set; }
        public string EventData { get; set; }
        public string Metadata { get; set; }
    }
}
