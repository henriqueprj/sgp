﻿using System;
using System.Threading.Tasks;

namespace PJMT.SgpPlus.Core.Domain.Services
{
    public interface INomeacaoService
    {
        Task Nomear(Guid idPessoa, Guid idCargo);
    }
}