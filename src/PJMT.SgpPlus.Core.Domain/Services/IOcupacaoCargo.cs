﻿using System;

namespace PJMT.SgpPlus.Core.Domain.Services
{
    public interface IOcupacaoCargoService
    {
        int ObterQuantidadeDisponiveisDoCargo(Guid idCargo);
    }

    public class OcupacaoCargoService : IOcupacaoCargoService
    {
        public int ObterQuantidadeDisponiveisDoCargo(Guid idCargo)
        {
            return 1; // *.*
        }
    }
}