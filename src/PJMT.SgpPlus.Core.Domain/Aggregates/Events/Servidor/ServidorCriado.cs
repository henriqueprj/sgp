﻿using System;
using EnjoyCQRS.Events;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Servidor
{
    public class ServidorCriado : DomainEvent
    {
        public ServidorCriado(Guid idPessoa) : base(idPessoa)
        {
        }
    }
}