﻿using System;
using EnjoyCQRS.Events;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Servidor
{
    public class ServidorNomeado : DomainEvent
    {
        public MovimentoNomeacao Nomeacao { get; }

        public ServidorNomeado(Guid id, MovimentoNomeacao nomeacao) : base(id)
        {
            Nomeacao = nomeacao;
        }
    }
}