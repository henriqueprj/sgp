﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa
{
    public class PessoaCriada : DomainEvent
    {
        public string Nome { get; }
        public string Cpf { get; }
        public DateTime DataNascimento { get; }

        public PessoaCriada(Guid aggregateId, string nome, string cpf, DateTime dataNascimento) 
            : base(aggregateId)
        {
            Nome = nome;
            Cpf = cpf;
            DataNascimento = dataNascimento;
        }
    }
}
