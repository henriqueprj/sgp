﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa
{
    public sealed class PessoaExcluida : DomainEvent
    {
        public DateTime ExcluidaEm { get; }

        public PessoaExcluida(Guid aggregateId, DateTime excluidaEm) : base(aggregateId)
        {
            ExcluidaEm = excluidaEm;
        }
    }
}
