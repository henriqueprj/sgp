﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Cargo
{
    public class CargoCriado : DomainEvent
    {
        public string Nome { get; }
        public int Quantidade { get; }

        public CargoCriado(Guid aggregateId, string nome, int quantidade) : base(aggregateId)
        {
            Nome = nome;
            Quantidade = quantidade;
        }
    }
}
