﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.EventSource;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using PJMT.SgpPlus.Core.Domain.ValueObjects;

namespace PJMT.SgpPlus.Core.Domain.Aggregates
{
    public class Pessoa : Aggregate
    {
        public Pessoa(Guid id, string nome, string cpf, DateTime dataNascimento)
        {
            if (string.IsNullOrEmpty(nome)) throw new ArgumentNullException(nameof(nome));

            Emit(new PessoaCriada(id, nome, cpf, dataNascimento));
        }

        public Pessoa()
        {
        }


        public string Nome { get; private set; }

        public string Cpf { get; private set; }

        public DateTime DataNascimento { get; private set; }

        public bool Excluida { get; private set; }

        public DateTime? ExcluidaEm { get; private set; }

        protected override void RegisterEvents()
        {
            SubscribeTo<PessoaCriada>(Aplicar);
            SubscribeTo<CpfAlterado>(Aplicar);
            SubscribeTo<PessoaExcluida>(Aplicar);
        }

        public void AlterarCpf(string novoCpf)
        {
            if (Excluida) throw new InvalidOperationException("A Pessoa encontra-se excluída.");

            if (novoCpf == null) throw new ArgumentNullException(nameof(novoCpf));
            if (novoCpf.Length != 11) throw new ArgumentOutOfRangeException(nameof(novoCpf), novoCpf, "O CPF informado não possui o tamanho válido (11 caracteres).");

            Emit(new CpfAlterado(Id, novoCpf));
        }

        public void Excluir()
        {
            if (Excluida) throw new InvalidOperationException("A Pessoa já encontra-se excluída.");

            Emit(new PessoaExcluida(Id, DateTime.Now));
        }

        private void Aplicar(PessoaCriada evento)
        {
            Id = evento.AggregateId;
            Nome = evento.Nome;
            Cpf = evento.Cpf;
            DataNascimento = evento.DataNascimento;
        }

        private void Aplicar(CpfAlterado evento)
        {
            Cpf = evento.NovoCpf;
        }

        private void Aplicar(PessoaExcluida evento)
        {
            Excluida = true;
            ExcluidaEm = evento.ExcluidaEm;
        }
    }
}
