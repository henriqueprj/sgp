﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJMT.SgpPlus.Core.Domain.ValueObjects
{
    public class Matricula
    {
        private readonly int _matricula;

        public Matricula(int matricula)
        {
            _matricula = matricula;
        }

        public override string ToString()
        {
            return _matricula.ToString();
        }

        public override bool Equals(object obj)
        {
            var other = obj as Matricula;
            return _matricula == other?._matricula;
        }

        public override int GetHashCode()
        {
            return _matricula.GetHashCode();
        }

        public static implicit operator Matricula(int matricula)
        {
            return new Matricula(matricula);
        }

        public static implicit operator int(Matricula matricula)
        {
            return matricula._matricula;
        }
    }
}
