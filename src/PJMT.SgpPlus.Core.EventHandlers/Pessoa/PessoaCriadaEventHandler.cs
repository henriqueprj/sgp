﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;

namespace PJMT.SgpPlus.Core.EventHandlers.Pessoa
{
    public class PessoaCriadaEventHandler : IEventHandler<PessoaCriada>
    {
        private readonly IPessoaRepository _pessoaRepository;

        public PessoaCriadaEventHandler(IPessoaRepository pessoaRepository)
        {
            _pessoaRepository = pessoaRepository;
        }

        public Task ExecuteAsync(PessoaCriada @event)
        {
            var pessoa = new Domain.ReadModel.Entities.Pessoa
            {
                DataCriacao = DateTime.Now,
                AggregateId = @event.AggregateId,
                Nome = @event.Nome,
                Cpf = @event.Cpf,
                DataNascimento = @event.DataNascimento
            };

            _pessoaRepository.Add(pessoa);

            return Task.CompletedTask;
        }
    }
}
