﻿using Microsoft.Owin.Cors;
using Owin;

namespace PJMT.SgpPlus.Core.ReadModel.Api
{
    public static class CorsConfig
    {
        public static void Configure(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
        }
    }
}