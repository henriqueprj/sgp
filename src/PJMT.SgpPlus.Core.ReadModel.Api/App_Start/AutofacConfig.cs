﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Windows.Input;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Features.Scanning;
using Autofac.Integration.WebApi;
using Owin;
using PJMT.SgpPlus.Core.Infrastructure.Data;
using PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP;
using PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Repositories;

namespace PJMT.SgpPlus.Core.ReadModel.Api
{
    public class AutofacConfig
    {
        internal static void Configure(IAppBuilder app, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            // Registra os Controllers (Api's).
            builder
                .RegisterApiControllers(typeof(Startup).Assembly)
                .InstancePerRequest();

            RegisterDependencies(builder);

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
        }

        private static void RegisterDependencies(ContainerBuilder builder)
        {
            builder.Register(c =>
            {
                var connectionString = ConfigurationManager.ConnectionStrings["SGP"]?.ConnectionString;
                if (string.IsNullOrEmpty(connectionString))
                    throw new ArgumentNullException("nameOrConnectionString", "Não foi possível obter a ConenctionString para SgpDbContext.");

                return new SgpDbContext(connectionString);
            }).AsSelf().InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(Infrastructure.EntityFramework.Foo).Assembly)
                .Where(x => x.FullName.StartsWith("PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Repositories.") &&
                            x.FullName.EndsWith("Repository"))
                .AsImplementedInterfaces();
        }
    }
}