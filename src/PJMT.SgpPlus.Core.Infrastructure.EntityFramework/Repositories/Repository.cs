﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using PJMT.SgpPlus.Core.Infrastructure.Data;
using PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP;

namespace PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected readonly SgpDbContext Context;
        private readonly DbSet<TEntity> _entitySet;

        protected Repository(SgpDbContext context)
        {
            Context = context;
            _entitySet = context.Set<TEntity>();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await _entitySet.FindAsync(id);
        }

        public async Task<TEntity> GetByAggregateIdAsync(Guid aggregateId)
        {
            return await _entitySet.SingleOrDefaultAsync(x => x.AggregateId == aggregateId);
        }

        public void Add(TEntity entity)
        {
            _entitySet.Add(entity);
        }

        public void Remove(TEntity entity)
        {
            _entitySet.Remove(entity);
        }
    }
}