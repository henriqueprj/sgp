﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;
using PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP;

namespace PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Repositories
{
    public class ServidorRepository : Repository<Servidor>, IServidorRepository
    {
        public ServidorRepository(SgpDbContext context) : base(context)
        {
        }
    }
}
