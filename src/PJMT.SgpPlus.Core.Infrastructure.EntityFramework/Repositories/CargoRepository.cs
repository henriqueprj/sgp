﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;
using PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP;

namespace PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Repositories
{
    public class CargoRepository : Repository<Cargo>, ICargoRepository
    {
        public CargoRepository(SgpDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Cargo>> GetAllAsync()
        {
            return await Context.Cargo.ToListAsync();
        }
    }
}