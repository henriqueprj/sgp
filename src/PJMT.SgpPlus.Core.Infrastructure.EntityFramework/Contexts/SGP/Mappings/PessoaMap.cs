﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;

namespace PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP.Mappings
{
    class PessoaMap : EntityTypeConfiguration<Pessoa>
    {
        public PessoaMap()
        {
            ToTable("Pessoa", "rm");

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.AggregateId)
                .HasColumnName("AggregateId");

            Property(x => x.DataCriacao)
                .HasColumnName("DataCriacao");

            Property(x => x.Nome)
                .HasColumnName("Nome")
                .HasMaxLength(50)
                .IsUnicode(false);

            Property(x => x.Cpf)
                .HasColumnName("CPF")
                .HasMaxLength(11)
                .IsUnicode(false)
                .IsFixedLength();

            Property(x => x.DataNascimento)
                .HasColumnName("DataNascimento");
        }
    }
}
