﻿using System;
using EnjoyCQRS.Commands;

namespace PJMT.SgpPlus.Core.Application.Pessoa
{
    public class ExcluirPessoaCommand : Command
    {
        public ExcluirPessoaCommand(Guid aggregateId) : base(aggregateId)
        {
        }
    }
}