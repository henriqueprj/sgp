using System.Threading.Tasks;
using EnjoyCQRS.Commands;
using EnjoyCQRS.EventSource.Exceptions;
using EnjoyCQRS.EventSource.Storage;

namespace PJMT.SgpPlus.Core.Application.Pessoa
{
    public class AlterarCpfCommandHandler : ICommandHandler<AlterarCpfCommand>
    {
        private readonly IRepository _repository;

        public AlterarCpfCommandHandler(IRepository repository)
        {
            _repository = repository;
        }
        
        public async Task ExecuteAsync(AlterarCpfCommand command)
        {
            var pessoa = await _repository.GetByIdAsync<Domain.Aggregates.Pessoa>(command.AggregateId);
            if (pessoa == null) throw new AggregateNotFoundException(nameof(Domain.Aggregates.Pessoa), command.AggregateId);

            pessoa.AlterarCpf(command.NovoCpf);
        }
    }
}