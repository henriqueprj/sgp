﻿using System.Threading.Tasks;
using EnjoyCQRS.Commands;
using EnjoyCQRS.EventSource.Storage;

namespace PJMT.SgpPlus.Core.Application.Pessoa
{
    public class CriarPessoaCommandHandler : ICommandHandler<CriarPessoaCommand>
    {
        private readonly IRepository _repository;

        public CriarPessoaCommandHandler(IRepository repository)
        {
            _repository = repository;
        }
        
        public async Task ExecuteAsync(CriarPessoaCommand command)
        {
            var pessoa = new Domain.Aggregates.Pessoa(command.AggregateId, command.Nome, command.Cpf, command.DataNascimento);
            await _repository.AddAsync(pessoa);
        }
    }
}
